<?php

/**
 * Description of admin
 *
 * @author terminus
 */
class Admin extends TT_Controller {

    private $assign;
    private $page_config;

    public function __construct() {
        parent::__construct();
        if ('admin' !== $this->ugroup) {
            message(lang('notice'), lang('err_nologin'), site_url('login'));
        }

        $this->load->language('ui/admin');

        $this->assign = array(
            'title' => lang('ui_admin'),
        );
        //读取分页配置
        $this->page_config = $this->config->item('page_config');
    }

    public function index() {
        $this->loadview('admin/menus', $this->assign);
    }

    public function user_index($page = 1) {
        //取整并判断页码
        $page = intval($page);
        if ($page < 1) {
            $page = 1;
        }

        $this->load->model('user_model', 'user');

        //分页
        $config = array_merge($this->page_config, array(
            'base_url' => site_url('admin/user_index'),
            'total_rows' => $this->user->getUsers(1),
        ));

        $this->pagination->initialize($config);
        $this->assign['pages'] = $this->pagination->create_links();
        $this->assign['users'] = $this->user->getUsers(0, $config['per_page'], (($page - 1) * $config['per_page']));

        $this->loadview(array('admin/menus', 'admin/user/index'), $this->assign);
    }

    public function user_add() {
        $this->loadview(array('admin/menus', 'admin/user/add'), $this->assign);
    }

    public function user_doadd() {
        $uname = $this->input->post('username');
        $password = $this->input->post('password');
        $re_password = $this->input->post('re_password');
        $role = trim($this->input->post('role'));

        if (!chkval('text', $uname, 6, 20)) {
            message(lang('notice'), lang('err_username_not_match_rule'), site_url('admin/user_add'));
        }

        if ($password !== $re_password) {
            message(lang('notice'), lang('err_password_repassword_not_match'), site_url('admin/user_add'));
        }

        if (!chkval('password', $password, 6, 20)) {
            message(lang('notice'), lang('err_password_not_match_rule'), site_url('admin/user_add'));
        }

        $this->load->model('user_model', 'user');
        if ($this->user->getUser($uname)) {
            message(lang('notice'), lang('err_username_already_used'), site_url('admin/user_add'));
        }

        switch ($role) {
            case '1':
                $role = 'admin';
                break;
            default:
                $role = '';
                break;
        }

        if ($this->user->addUser($uname, $password, $role) > 0) {
            message(lang('notice'), lang('user_add_success'), site_url('admin/user_index'));
        }
        message(lang('error'), lang('err_user_add_fail'), site_url('admin/user_add'));
    }

    public function user_dodel($uid) {
        $this->load->model('user_model', 'user');

        if (FALSE == $this->user->getUser($uid, 1)) {
            message(lang('error'), lang('err_user_not_exist'), site_url('admin/user_index'));
        }

        if (FALSE == $this->user->delUser($uid)) {
            message(lang('error'), lang('err_user_del_fail'), site_url('admin/user_index'));
        }

        message(lang('notice'), lang('user_del_success'), site_url('admin/user_index'));
    }

    public function user_edit($uid) {
        $this->load->model('user_model', 'user');

        $u = $this->user->getUser($uid, 1);
        if (FALSE == $u) {
            message(lang('error'), lang('err_user_not_exist'), site_url('admin/user_index'));
        }

        $this->assign['user'] = $u;
        $this->loadview(array('admin/menus', 'admin/user/edit'), $this->assign);
    }

    public function user_doedit() {
        $uid = strip_some($this->input->post('uid'));
        $password = $this->input->post('password');
        $re_password = $this->input->post('re_password');
        $role = strip_some($this->input->post('role'));

        $this->load->model('user_model', 'user');

        if ('' !== $password) {
            if ($password !== $re_password) {
                message(lang('notice'), lang('err_password_repassword_not_match'), site_url('admin/user_edit/' . $uid));
            }

            if (!chkval('password', $password, 6, 20)) {
                message(lang('notice'), lang('err_password_not_match_rule'), site_url('admin/user_edit/' . $uid));
            }
        }

        switch ($role) {
            case '1':
                $role = 'admin';
                break;
            default:
                $role = '';
                break;
        }

        if ($this->user->setUser($uid, $password, $role)) {
            message(lang('notice'), lang('user_edit_success'), site_url('admin/user_index'));
        }

        message(lang('error'), lang('err_user_edit_fail'), site_url('admin/user_edit/' . $uid));
    }

    public function blacklists_index($page = 1) {
        //取整并判断页码
        $page = intval($page);
        if ($page < 1) {
            $page = 1;
        }

        $this->load->model('blacklists_model', 'bl');

        //分页
        $config = array_merge($this->page_config, array(
            'base_url' => site_url('admin/blacklists_index'),
            'total_rows' => $this->bl->getAll(1),
        ));

        $this->pagination->initialize($config);
        $this->assign['pages'] = $this->pagination->create_links();

        $this->assign['blacklists'] = $this->bl->getAll(0, $config['per_page'], (($page - 1) * $config['per_page']));

        $this->loadview(array('admin/menus', 'admin/blacklists/index'), $this->assign);
    }

    public function blacklists_add() {
        $this->loadview(array('admin/menus', 'admin/blacklists/add'), $this->assign);
    }

    public function blacklists_doadd() {
        $user_opn_id = strip_some($this->input->post('user_opn_id'));

        if (!chkval('text', $user_opn_id, 28, 28)) {
            message(lang('notice'), lang('err_user_opn_id_not_match_rule'), site_url('admin/blacklists_add'));
        }

        $this->load->model('blacklists_model', 'bl');

        if ($this->bl->add($user_opn_id) > 0) {
            message(lang('notice'), lang('blacklists_add_success'), site_url('admin/blacklists_index'));
        }
        message(lang('error'), lang('err_blacklists_add_failed'), site_url('admin/blacklists_add'));
    }

    public function blacklists_dodel($user_opn_id) {
        $this->load->model('blacklists_model', 'bl');

        if (FALSE == $this->bl->get($user_opn_id)) {
            message(lang('error'), lang('err_blacklists_not_exist'), site_url('admin/blacklists_index'));
        }

        if (FALSE == $this->bl->del($user_opn_id)) {
            message(lang('error'), lang('err_blacklists_del_failed'), site_url('admin/blacklists_index'));
        }

        message(lang('notice'), lang('blacklists_del_success'), site_url('admin/blacklists_index'));
    }

    public function msgtype_index($page = 1) {
        //取整并判断页码
        $page = intval($page);
        if ($page < 1) {
            $page = 1;
        }

        $this->load->model('msgtype_model', 'mt');

        //分页
        $config = array_merge($this->page_config, array(
            'base_url' => site_url('admin/msgtype_index'),
            'total_rows' => $this->mt->getMsgtypes(1),
        ));

        $this->pagination->initialize($config);
        $this->assign['pages'] = $this->pagination->create_links();

        $this->assign['msgtype'] = $this->mt->getMsgtypes(0, NULL, $config['per_page'], (($page - 1) * $config['per_page']));

        $this->loadview(array('admin/menus', 'admin/msgtype/index'), $this->assign);
    }

    public function msgtype_add() {
        $this->loadview(array('admin/menus', 'admin/msgtype/add'), $this->assign);
    }

    public function msgtype_doadd() {
        $type_name = strip_some($this->input->post('type_name'));
        $desc = strip_some($this->input->post('description'));
        $for_reply = strip_some($this->input->post('for_reply'));

        $this->load->model('msgtype_model', 'mt');

        if (!chkval('text', $type_name, 1, 10)) {
            message(lang('error'), lang('err_msgtype_not_match_rule'), site_url('admin/msgtype_add'));
        }

        if (($for_reply != 0) && ($for_reply != 1)) {
            message(lang('error'), lang('err_invalid_data'), site_url('admin/msgtype_add'));
        }

        if ($this->mt->getMsgtype($type_name, 0)) {
            message(lang('error'), lang('err_msgtype_already_exist'), site_url('admin/msgtype_add'));
        }

        if ($this->mt->addMsgtype($type_name, $desc, $for_reply) > 0) {
            message(lang('notice'), lang('msgtype_add_success'), site_url('admin/msgtype_index'));
        }
        message(lang('error'), lang('err_msgtype_add_failed'), site_url('admin/msgtype_add'));
    }

    public function msgtype_dodel($msgtype_id) {
        $this->load->model('msgtype_model', 'mt');

        if (FALSE == $this->mt->getMsgtype($msgtype_id)) {
            message(lang('error'), lang('err_msgtype_not_exist'), site_url('admin/msgtype_index'));
        }

        if (FALSE == $this->mt->delMsgtype($msgtype_id)) {
            message(lang('error'), lang('err_msgtype_del_failed'), site_url('admin/msgtype_index'));
        }

        message(lang('notice'), lang('msgtype_del_success'), site_url('admin/msgtype_index'));
    }

    public function msgtype_edit($msgtype_id) {
        $this->load->model('msgtype_model', 'mt');

        $m = $this->mt->getMsgtype($msgtype_id);
        if (FALSE == $m) {
            message(lang('error'), lang('err_msgtype_not_exist'), site_url('admin/msgtype_index'));
        }

        $this->assign['msgtype'] = $m;
        $this->loadview(array('admin/menus', 'admin/msgtype/edit'), $this->assign);
    }

    public function msgtype_doedit() {
        $id = strip_some($this->input->post('id'));
        $type_name = strip_some($this->input->post('type_name'));
        $desc = strip_some($this->input->post('description'));
        $for_reply = strip_some($this->input->post('for_reply'));

        $this->load->model('msgtype_model', 'mt');
        if (!$this->mt->getMsgtype($id)) {
            message(lang('error'), lang('err_msgtype_not_exist'), site_url('admin/msgtype_index'));
        }

        if (!chkval('text', $type_name, 1, 10)) {
            message(lang('error'), lang('err_msgtype_not_match_rule'), site_url('admin/msgtype_edit/' . $id));
        }

        if (($for_reply != 0) && ($for_reply != 1)) {
            message(lang('error'), lang('err_invalid_data'), site_url('admin/msgtype_edit/' . $id));
        }

        if ($this->mt->editMsgtype($id, $type_name, $desc, $for_reply)) {
            message(lang('notice'), lang('msgtype_edit_success'), site_url('admin/msgtype_index'));
        }
        message(lang('error'), lang('err_msgtype_edit_failed'), site_url('admin/msgtype_edit/' . $id));
    }

    public function keywords_index($page = 1) {
        //取整并判断页码
        $page = intval($page);
        if ($page < 1) {
            $page = 1;
        }

        $this->load->model('keywords_model', 'kw');

        //分页
        $config = array_merge($this->page_config, array(
            'base_url' => site_url('admin/keywords_index'),
            'total_rows' => $this->kw->getKeywords(1),
        ));

        $this->pagination->initialize($config);
        $this->assign['pages'] = $this->pagination->create_links();

        $keywords = $this->kw->getKeywords(0, NULL, $config['per_page'], (($page - 1) * $config['per_page']));
        //写入对应类型名
        $this->load->model('msgtype_model', 'mt');
        foreach ($keywords as $v) {
            $v->for_msgtype_name = $this->mt->getMsgtype($v->for_msgtype_id)->type_name;
        }

        $this->assign['keywords'] = $keywords;
        $this->loadview(array('admin/menus', 'admin/keywords/index'), $this->assign);
    }

    public function keywords_add() {
        $this->load->model('msgtype_model', 'mt');
        $this->assign['msgtype'] = $this->mt->getMsgtypes(0, NULL);
        $this->loadview(array('admin/menus', 'admin/keywords/add'), $this->assign);
    }

    public function keywords_doadd() {
        $for_msgtype_id = strip_some($this->input->post('for_msgtype_id'));
        $keyword = strip_some($this->input->post('keyword'));
        $desc = strip_some($this->input->post('description'));

        $this->load->model('msgtype_model', 'mt');
        if (!$this->mt->getMsgtype($for_msgtype_id)) {
            message(lang('error'), lang('err_msgtype_not_exist'), site_url('admin/keywords_add'));
        }

        if (!mb_strlen($keyword, 'UTF-8') > 100) {
            message(lang('error'), lang('err_keyword_not_match_rule'), site_url('admin/keywords_add'));
        }

        $this->load->model('keywords_model', 'kw');

        if ($this->kw->addKeyword($for_msgtype_id, $keyword, $desc) > 0) {
            message(lang('notice'), lang('keyword_add_success'), site_url('admin/keywords_index'));
        }
        message(lang('error'), lang('err_keyword_add_failed'), site_url('admin/keywords_add'));
    }

    public function keywords_edit($id) {
        $this->load->model('keywords_model', 'kw');

        $k = $this->kw->getKeyword($id)[0];
        if (FALSE == $k) {
            message(lang('error'), lang('err_keyword_not_exist'), site_url('admin/keywords_index'));
        }

        $this->assign['keyword'] = $k;
        $this->load->model('msgtype_model', 'mt');
        $this->assign['msgtype'] = $this->mt->getMsgtypes(0, NULL);
        $this->loadview(array('admin/menus', 'admin/keywords/edit'), $this->assign);
    }

    public function keywords_doedit() {
        $id = strip_some($this->input->post('id'));
        $for_msgtype_id = strip_some($this->input->post('for_msgtype_id'));
        $keyword = strip_some($this->input->post('keyword'));
        $desc = strip_some($this->input->post('description'));

        $this->load->model('keywords_model', 'kw');

        if (!$this->kw->getKeyword($id)[0]) {
            message(lang('error'), lang('err_keyword_not_exist'), site_url('admin/keywords_index'));
        }

        $this->load->model('msgtype_model', 'mt');
        if (!$this->mt->getMsgtype($for_msgtype_id)) {
            message(lang('error'), lang('err_msgtype_not_exist'), site_url('admin/keywords_edit/' . $id));
        }

        if (!mb_strlen($keyword, 'UTF-8') > 100) {
            message(lang('error'), lang('err_keyword_not_match_rule'), site_url('admin/keywords_edit/' . $id));
        }

        if ($this->kw->editKeyword($id, $keyword, $desc, $for_msgtype_id)) {
            message(lang('notice'), lang('keyword_edit_success'), site_url('admin/keywords_index'));
        }
        message(lang('error'), lang('err_keyword_edit_failed'), site_url('admin/keywords_edit/' . $id));
    }

    public function keywords_dodel($id) {
        $this->load->model('keywords_model', 'kw');

        if (FALSE == $this->kw->getKeyword($id, 1)) {
            message(lang('error'), lang('err_keyword_not_exist'), site_url('admin/keywords_index'));
        }

        if (FALSE == $this->kw->delKeyword($id)) {
            message(lang('error'), lang('err_keyword_del_failed'), site_url('admin/keywords_index'));
        }

        message(lang('notice'), lang('keyword_del_success'), site_url('admin/keywords_index'));
    }

    public function reply_index($page = 1) {
        //取整并判断页码
        $page = intval($page);
        if ($page < 1) {
            $page = 1;
        }

        $this->load->model('reply_model', 'rp');

        //分页
        $config = array_merge($this->page_config, array(
            'base_url' => site_url('admin/reply_index'),
            'total_rows' => $this->rp->getAll(1),
        ));

        $this->pagination->initialize($config);
        $this->assign['pages'] = $this->pagination->create_links();

        $r = $this->rp->getAll(0, $config['per_page'], (($page - 1) * $config['per_page']));
        //写入对应关键字名
        $this->load->model('keywords_model', 'kw');
        //写入对应类型名
        $this->load->model('msgtype_model', 'mt');
        foreach ($r as $v) {
            $v->msgtype_name = $this->mt->getMsgtype($v->reply_msgtype_id)->type_name;
            $v->for_keyword = $this->kw->getKeyword($v->for_key_id)[0]->keyword;
        }

        $this->assign['reply'] = $r;
        $this->loadview(array('admin/menus', 'admin/reply/index'), $this->assign);
    }

    public function reply_add() {
        $this->load->model('keywords_model', 'kw');
        $this->load->model('msgtype_model', 'mt');
        $this->assign['keywords'] = $this->kw->getKeywords();
        $this->assign['msgtype'] = $this->mt->getMsgtypes(0, 1);

        $this->loadview(array('admin/menus', 'admin/reply/add'), $this->assign);
    }

    public function reply_doadd() {
        //检查关键字
        $kid = strip_some($this->input->post('keyword_id'));
        $this->load->model('keywords_model', 'kw');
        $k = $this->kw->getKeyword($kid)[0];
        if (!$k) {
            message(lang('error'), lang('err_keyword_not_exist'), site_url('admin/reply_add'));
        }

        $reply_msgtype_id = strip_some($this->input->post('reply_msgtype_id'));
        $this->load->model('msgtype_model', 'mt');
        //检查消息类型是否存在及是否可用于回复
        $mt = $this->mt->getMsgtype($reply_msgtype_id);
        if (!$mt || ($mt->for_reply == 0)) {
            message(lang('error'), lang('err_msgtype_not_exist'), site_url('admin/reply_add'));
        }

        $content = strip_some($this->input->post('content'));

        $pic_url = strip_some($this->input->post('pic_url'));
        $link_url = strip_some($this->input->post('link_url'));

        $music_url = strip_some($this->input->post('music_url'));
        $hqmusic_url = strip_some($this->input->post('hqmusic_url'));

        switch ($mt->type_name) {
            case 'music':
                $title = strip_some($this->input->post('music_title'));
                $description = strip_some($this->input->post('music_description'));
                break;
            case 'news':
                $title = strip_some($this->input->post('news_title'));
                $description = strip_some($this->input->post('news_description'));
                break;
            default:
                $title = '';
                $description = '';
                break;
        }

        $this->load->model('reply_model', 'rp');
        if ($this->rp->addReply($kid, $reply_msgtype_id, $content, $title, $description, $pic_url, $link_url, $music_url, $hqmusic_url) > 0) {
            message(lang('notice'), lang('reply_add_success'), site_url('admin/reply_index'));
        }
        message(lang('error'), lang('err_reply_add_failed'), site_url('admin/reply_index'));
    }

    public function reply_dodel($id) {
        $this->load->model('reply_model', 'rp');

        if (FALSE == $this->rp->getReply($id)) {
            message(lang('error'), lang('err_reply_not_exist'), site_url('admin/reply_index'));
        }

        if (FALSE == $this->rp->delReply($id)) {
            message(lang('error'), lang('err_reply_del_failed'), site_url('admin/reply_index'));
        }

        message(lang('notice'), lang('reply_del_success'), site_url('admin/reply_index'));
    }

    public function reply_view($id) {
        $this->load->model('reply_model', 'rp');

        $r = $this->rp->getReply($id);
        if (FALSE == $r) {
            message(lang('error'), lang('err_reply_not_exist'), site_url('admin/reply_index'));
        }

        //写入对应关键字名
        $this->load->model('keywords_model', 'kw');
        //写入对应类型名
        $this->load->model('msgtype_model', 'mt');
        $r->msgtype_name = $this->mt->getMsgtype($r->reply_msgtype_id)->type_name;
        $r->for_keyword = $this->kw->getKeyword($r->for_key_id)[0]->keyword;

        $this->assign['reply'] = $r;
        $this->loadview(array('admin/menus', 'admin/reply/view'), $this->assign);
    }

    public function reply_edit($id) {
        $this->load->model('reply_model', 'rp');

        $r = $this->rp->getReply($id);
        if (FALSE == $r) {
            message(lang('error'), lang('err_reply_not_exist'), site_url('admin/reply_index'));
        }

        //写入对应关键字名
        $this->load->model('keywords_model', 'kw');
        //写入对应类型名
        $this->load->model('msgtype_model', 'mt');
        $r->msgtype_name = $this->mt->getMsgtype($r->reply_msgtype_id)->type_name;
        $r->for_keyword = $this->kw->getKeyword($r->for_key_id)[0]->keyword;

        $this->assign['reply'] = $r;
        $this->assign['keywords'] = $this->kw->getKeywords();
        $this->assign['msgtype'] = $this->mt->getMsgtypes(0, 1);
        $this->loadview(array('admin/menus', 'admin/reply/edit'), $this->assign);
    }

    public function reply_doedit() {
        $id = strip_some($this->input->post('id'));
        //检查关键字
        $kid = strip_some($this->input->post('keyword_id'));
        $this->load->model('keywords_model', 'kw');
        $k = $this->kw->getKeyword($kid)[0];
        if (!$k) {
            message(lang('error'), lang('err_keyword_not_exist'), site_url('admin/reply_edit/' . $id));
        }

        $reply_msgtype_id = strip_some($this->input->post('reply_msgtype_id'));
        $this->load->model('msgtype_model', 'mt');
        //检查消息类型是否存在及是否可用于回复
        $mt = $this->mt->getMsgtype($reply_msgtype_id);
        if (!$mt || ($mt->for_reply == 0)) {
            message(lang('error'), lang('err_msgtype_not_exist'), site_url('admin/reply_edit/' . $id));
        }

        $content = strip_some($this->input->post('content'));

        $pic_url = strip_some($this->input->post('pic_url'));
        $link_url = strip_some($this->input->post('link_url'));

        $music_url = strip_some($this->input->post('music_url'));
        $hqmusic_url = strip_some($this->input->post('hqmusic_url'));

        switch ($mt->type_name) {
            case 'music':
                $title = strip_some($this->input->post('music_title'));
                $description = strip_some($this->input->post('music_description'));
                break;
            case 'news':
                $title = strip_some($this->input->post('news_title'));
                $description = strip_some($this->input->post('news_description'));
                break;
            default:
                $title = '';
                $description = '';
                break;
        }

        $this->load->model('reply_model', 'rp');
        if ($this->rp->editReply($id, $kid, $reply_msgtype_id, $content, $title, $description, $pic_url, $link_url, $music_url, $hqmusic_url) > 0) {
            message(lang('notice'), lang('reply_edit_success'), site_url('admin/reply_index'));
        }
        message(lang('error'), lang('err_reply_edit_failed'), site_url('admin/reply_index'));
    }

    public function commands_index($page = 1) {
        //取整并判断页码
        $page = intval($page);
        if ($page < 1) {
            $page = 1;
        }

        $this->load->model('commands_model', 'cmd');

        //分页
        $config = array_merge($this->page_config, array(
            'base_url' => site_url('admin/commands_index'),
            'total_rows' => $this->cmd->getAll(1),
        ));

        $this->pagination->initialize($config);
        $this->assign['pages'] = $this->pagination->create_links();

        $c = $this->cmd->getAll(0, $config['per_page'], (($page - 1) * $config['per_page']));
        //写入对应类型名
        //写入父命令
        $this->load->model('msgtype_model', 'mt');
        foreach ($c as $v) {
            $v->reply_msgtype_name = $this->mt->getMsgtype($v->reply_msgtype_id)->type_name;
            if ($v->p_cmd_id > 0) {
                $v->parent_command = $this->cmd->get($v->p_cmd_id, 1)->command;
            }
        }

        $this->assign['commands'] = $c;
        $this->loadview(array('admin/menus', 'admin/commands/index'), $this->assign);
    }

    public function commands_add() {
        $this->load->model('commands_model', 'cmd');
        $this->load->model('msgtype_model', 'mt');
        $this->assign['commands'] = $this->cmd->getAll(0);
        $this->assign['msgtype'] = $this->mt->getMsgtypes(0, 1);
        $this->loadview(array('admin/menus', 'admin/commands/add'), $this->assign);
    }

    public function commands_doadd() {
        //检查父命令
        $pid = intval(strip_some($this->input->post('parent_command_id')));
        $this->load->model('commands_model', 'cmd');
        if ($pid > 0) {
            $p = $this->cmd->get($pid);
            if (!$p) {
                message(lang('error'), lang('err_command_not_exist'), site_url('admin/commands_add'));
            }
        }

        $reply_msgtype_id = strip_some($this->input->post('reply_msgtype_id'));
        $this->load->model('msgtype_model', 'mt');
        //检查消息类型是否存在及是否可用于回复
        $mt = $this->mt->getMsgtype($reply_msgtype_id);
        if (!$mt || ($mt->for_reply == 0)) {
            message(lang('error'), lang('err_msgtype_not_exist'), site_url('admin/commands_add'));
        }

        $command = strip_some($this->input->post('command'));
        $data_regex = strip_some($this->input->post('data_regex'));
        $is_expire = strip_some($this->input->post('is_expire'));
        $is_with_plugin = strip_some($this->input->post('is_with_plugin'));
        $plugin_name = strip_some($this->input->post('plugin_name'));
        $plugin_function = strip_some($this->input->post('plugin_function'));

        $content = strip_some($this->input->post('content'));

        $pic_url = strip_some($this->input->post('pic_url'));
        $link_url = strip_some($this->input->post('link_url'));

        $music_url = strip_some($this->input->post('music_url'));
        $hqmusic_url = strip_some($this->input->post('hqmusic_url'));

        switch ($mt->type_name) {
            case 'music':
                $title = strip_some($this->input->post('music_title'));
                $description = strip_some($this->input->post('music_description'));
                break;
            case 'news':
                $title = strip_some($this->input->post('news_title'));
                $description = strip_some($this->input->post('news_description'));
                break;
            default:
                $title = '';
                $description = '';
                break;
        }

        if ($this->cmd->add($command, $pid, $data_regex, $is_expire, $is_with_plugin, $plugin_name, $plugin_function, $reply_msgtype_id, $content, $title, $description, $pic_url, $link_url, $music_url, $hqmusic_url) > 0) {
            message(lang('notice'), lang('command_add_success'), site_url('admin/commands_index'));
        }
        message(lang('error'), lang('err_command_add_failed'), site_url('admin/commands_index'));
    }

    public function commands_view($id) {
        $this->load->model('commands_model', 'cmd');

        $c = $this->cmd->get($id);
        if (FALSE == $c) {
            message(lang('error'), lang('err_command_not_exist'), site_url('admin/commands_index'));
        }

        //写入回复类型及判断写入父命令
        $this->load->model('msgtype_model', 'mt');
        $c->reply_msgtype_name = $this->mt->getMsgtype($c->reply_msgtype_id)->type_name;
        if ($c->p_cmd_id > 0) {
            $c->parent_command = $this->cmd->get($c->p_cmd_id)->command;
        }

        $this->assign['command'] = $c;
        $this->loadview(array('admin/menus', 'admin/commands/view'), $this->assign);
    }

    public function commands_edit($id) {
        $this->load->model('commands_model', 'cmd');

        $c = $this->cmd->get($id);
        if (FALSE == $c) {
            message(lang('error'), lang('err_command_not_exist'), site_url('admin/commands_index'));
        }

        //写入对应类型名,父命令
        $this->load->model('msgtype_model', 'mt');
        $c->reply_msgtype_name = $this->mt->getMsgtype($c->reply_msgtype_id)->type_name;
        if ($c->p_cmd_id > 0) {
            $c->parent_command = $this->cmd->get($c->p_cmd_id)->command;
        }

        $this->assign['command'] = $c;
        $this->assign['commands'] = $this->cmd->getAll();
        $this->load->model('keywords_model', 'kw');
        $this->assign['keywords'] = $this->kw->getKeywords();
        $this->assign['msgtype'] = $this->mt->getMsgtypes(0, 1);
        $this->loadview(array('admin/menus', 'admin/commands/edit'), $this->assign);
    }

    public function commands_doedit() {
        //检查存在
        $id = intval(strip_some($this->input->post('id')));
        $this->load->model('commands_model', 'cmd');
        if ($id > 0) {
            $c = $this->cmd->get($id);
            if (!$c) {
                message(lang('error'), lang('err_command_not_exist'), site_url('admin/commands_edit/' . $id));
            }
        }

        //检查父命令
        $pid = intval(strip_some($this->input->post('parent_command_id')));
        if ($pid > 0) {
            $p = $this->cmd->get($pid);
            if (!$p) {
                message(lang('error'), lang('err_command_not_exist'), site_url('admin/commands_edit/' . $id));
            }
        }

        $reply_msgtype_id = strip_some($this->input->post('reply_msgtype_id'));
        $this->load->model('msgtype_model', 'mt');
        //检查消息类型是否存在及是否可用于回复
        $mt = $this->mt->getMsgtype($reply_msgtype_id);
        if (!$mt || ($mt->for_reply == 0)) {
            message(lang('error'), lang('err_msgtype_not_exist'), site_url('admin/commands_edit/' . $id));
        }

        $command = strip_some($this->input->post('command'));
        $data_regex = strip_some($this->input->post('data_regex'));
        $is_expire = strip_some($this->input->post('is_expire'));
        $is_with_plugin = strip_some($this->input->post('is_with_plugin'));
        $plugin_name = strip_some($this->input->post('plugin_name'));
        $plugin_function = strip_some($this->input->post('plugin_function'));

        $content = strip_some($this->input->post('content'));

        $pic_url = strip_some($this->input->post('pic_url'));
        $link_url = strip_some($this->input->post('link_url'));

        $music_url = strip_some($this->input->post('music_url'));
        $hqmusic_url = strip_some($this->input->post('hqmusic_url'));

        switch ($mt->type_name) {
            case 'music':
                $title = strip_some($this->input->post('music_title'));
                $description = strip_some($this->input->post('music_description'));
                break;
            case 'news':
                $title = strip_some($this->input->post('news_title'));
                $description = strip_some($this->input->post('news_description'));
                break;
            default:
                $title = '';
                $description = '';
                break;
        }

        if ($this->cmd->edit($id, $command, $pid, $data_regex, $is_expire, $is_with_plugin, $plugin_name, $plugin_function, $reply_msgtype_id, $content, $title, $description, $pic_url, $link_url, $music_url, $hqmusic_url)) {
            message(lang('notice'), lang('command_edit_success'), site_url('admin/commands_index'));
        }
        message(lang('error'), lang('err_command_edit_failed'), site_url('admin/commands_edit/' . $id));
    }

    public function commands_dodel($id) {
        $this->load->model('commands_model', 'cmd');

        if (FALSE == $this->cmd->get($id)) {
            message(lang('error'), lang('err_command_not_exist'), site_url('admin/commands_index'));
        }

        if (FALSE == $this->cmd->del($id)) {
            message(lang('error'), lang('err_command_del_failed'), site_url('admin/commands_index'));
        }

        message(lang('notice'), lang('command_del_success'), site_url('admin/commands_index'));
    }

    public function ajax() {
        $do = $this->input->post('do');

        $rd = array(
            'status' => 'false'
        );
        switch ($do) {
            case 'getkeyword':
                $this->load->model('keywords_model', 'kw');
                $id = strip_some($this->input->post('id'));
                $k = $this->kw->getKeyword($id)[0];
                if ($k) {
                    $rd = array(
                        'status' => 'success',
                        'data' => $k->keyword,
                    );
                }
                break;
            case 'getcommand':
                $this->load->model('commands_model', 'cmd');
                $id = strip_some($this->input->post('id'));
                $c = $this->cmd->get($id);
                if ($c) {
                    $rd = array(
                        'status' => 'success',
                        'data' => $c->command,
                    );
                }
                break;
            default:
                $rd = array(
                    'status' => 'error',
                    'msg' => 'unknown commond'
                );
                break;
        }

        echo json_encode($rd);
    }

}