<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="<?php echo public_res('js/jquery-1.9.1.min.js'); ?>" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                var upload_result = $("#result").html();
                $(window.parent.document).find("#<?php echo (empty($result_field_id) ? 'upload_filename' : $result_field_id); ?>").val(upload_result);
            });
        </script>
    </head>
    <body>
        <div id="result"><?php echo (empty($result) ? 'err_file_upload_failed' : $result); ?></div>
    </body>
</html>
