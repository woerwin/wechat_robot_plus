<div id="win_r">
    <form action="<?php echo site_url('admin/keywords_doadd'); ?>" method="post">
        <ul>
            <li>
                <span class="title"><?php echo lang('for_msgtype'); ?></span>
                <select name="for_msgtype_id" class="input">
                    <?php
                    foreach ($msgtype as $v) {
                        $tmp = '<option value="' . $v->id . '">'
                                . $v->type_name
                                . '</option>';
                        echo $tmp;
                    }
                    ?>
                </select>
                <span class="m_left_10 notice"><?php echo lang('for_msgtype_note'); ?></span>
            </li>
            <li>
                <span class="title"><?php echo lang('keyword'); ?></span>
                <input type="text" name="keyword" value="" class="input" maxlength="100" />
            </li>
            <li>
                <span class="title"><?php echo lang('description'); ?></span>
                <textarea name="description" class="input_area" maxlength="100"></textarea>
            </li>
            <li class="text_c">
                <input type="submit" value="<?php echo lang('add'); ?>" onclick="submit();" />
            </li> 
        </ul>
    </form>
</div>