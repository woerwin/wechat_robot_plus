<div id="win_r">
    <table cellpadding="0" cellspacing="0" class="text_c">
        <tr class="table_title">
            <td style="width: 200px"><?php echo lang('command'); ?></td>
            <td style="width: 100px"><?php echo lang('parent_command'); ?></td>
            <td style="width: 100px"><?php echo lang('data_regex'); ?></td>
            <td style="width: 100px"><?php echo lang('reply_msgtype'); ?></td>
            <td style="width: 150px"><?php echo lang('operation'); ?></td>
        </tr>
        <?php
        if (is_array($commands)) {
            foreach ($commands as $v) {
                $tmp = '<tr>'
                        . '<td>' . $v->command . '</td>'
                        . '<td>' . (!empty($v->parent_command) ? $v->parent_command : '') . '</td>'
                        . '<td>' . $v->data_regex . '</td>'
                        . '<td>' . $v->reply_msgtype_name . '</td>'
                        . '<td>'
                        . anchor(site_url('admin/commands_view/' . $v->id), lang('view'))
                        . ' | '
                        . anchor(site_url('admin/commands_edit/' . $v->id), lang('edit'))
                        . ' | '
                        . anchor(site_url('admin/commands_dodel/' . $v->id), lang('delete'), array(
                            'onclick' => 'if(false===confirm(\'' . lang('confirm_to_delete') . '\')){return false;}'
                        ))
                        . '</td>';
                echo $tmp;
            }
        } else {
            echo lang('err_no_data');
        }
        ?>
    </table>
    <?php echo $pages; ?>
</div>